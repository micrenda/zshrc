
# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle sudo
antigen bundle colored-man-pages
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle smallhadroncollider/antigen-git-store
antigen bundle micrenda/zsh-nohup
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
#antigen theme agnoster
antigen theme romkatv/powerlevel10k

# Tell antigen that you're done.
antigen apply
